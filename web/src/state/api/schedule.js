import axios from "axios";
export const API_URL = process.env.REACT_APP_API_URL;

export const getSchedule = ({ date, sid}) => {
  return axios.get(`${API_URL}/schedule/${date}/${sid}`).then((response) => {
    return response.data;
  });
};
