import axios from "axios";
export const API_URL = process.env.REACT_APP_API_URL;

export const getServices = () => {
  return axios.get(`${API_URL}/services.json`).then((response) => {
    return response.data;
  });
};
