import { createSelector } from 'reselect'
import {API_URL} from "../api/schedule";

const getResults = (state) => state.services.data.results;

export const mapServicesData = createSelector(
    [getResults],
    (results) => {
        return results.map((item) => ({
            ...item,
            logo: `${API_URL}/tvguide/logos/${item.sid}/100x100.png`
        }))
    });
