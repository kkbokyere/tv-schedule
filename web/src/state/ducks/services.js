import { getServices } from "../api/services";

export const GET_SERVICES_REQUEST = "GET_SERVICES_REQUEST";
export const GET_SERVICES_SUCCESS = "GET_SERVICES_SUCCESS";
export const GET_SERVICES_FAILURE = "GET_SERVICES_FAILURE";

export const initialState = {
  isLoading: false,
  error: null,
  data: []
};

function getServicesRequest(payload) {
  return {
    type: GET_SERVICES_REQUEST,
    payload,
  };
}

function getServicesSuccess(payload) {
  return {
    type: GET_SERVICES_SUCCESS,
    payload,
  };
}

function getServicesFailure(payload) {
  return {
    type: GET_SERVICES_FAILURE,
    payload,
  };
}

export function fetchServices() {
  return function (dispatch) {
    dispatch(getServicesRequest());
    return getServices()
      .then((response) => {
        dispatch(getServicesSuccess(response));
      })
      .catch((error) => {
        dispatch(getServicesFailure(error));
      });
  };
}

export default (state = initialState, action) => {
  let { payload } = action;
  switch (action.type) {
    case GET_SERVICES_SUCCESS:
      return {
        ...state,
        data: payload,
        isLoading: false,
      };
    case GET_SERVICES_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload,
      };
    case GET_SERVICES_REQUEST:
      return {
        ...state,
        error: null,
        isLoading: true,
      };
    default:
      return state;
  }
};
