import { getSchedule } from "../api/schedule";

export const GET_SCHEDULE_REQUEST = "GET_SCHEDULE_REQUEST";
export const GET_SCHEDULE_SUCCESS = "GET_SCHEDULE_SUCCESS";
export const GET_SCHEDULE_FAILURE = "GET_SCHEDULE_FAILURE";

export const initialState = {
  isLoading: false,
  error: null,
  data: []
};

function getScheduleRequest(payload) {
  return {
    type: GET_SCHEDULE_REQUEST,
    payload,
  };
}

function getScheduleSuccess(payload) {
  return {
    type: GET_SCHEDULE_SUCCESS,
    payload,
  };
}

function getScheduleFailure(payload) {
  return {
    type: GET_SCHEDULE_FAILURE,
    payload,
  };
}

export function fetchSchedule(params) {
  return function (dispatch) {
    dispatch(getScheduleRequest(params));
    return getSchedule(params)
      .then((response) => {
        dispatch(getScheduleSuccess(response));
      })
      .catch((error) => {
        dispatch(getScheduleFailure(error));
      });
  };
}

export default (state = initialState, action) => {
  let { payload } = action;
  switch (action.type) {
    case GET_SCHEDULE_SUCCESS:
      return {
        ...state,
        data: payload,
        isLoading: false,
      };
    case GET_SCHEDULE_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload,
      };
    case GET_SCHEDULE_REQUEST:
      return {
        ...state,
        error: null,
        isLoading: true,
      };
    default:
      return state;
  }
};
