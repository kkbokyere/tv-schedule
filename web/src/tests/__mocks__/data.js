export const mockServicesResponse = [{
    sid: "2002",
    t: "BBC One Lon",
    c: "101",
    sf: "SD",
    sg: "Entertainment"
}];


export const mockScheduleResponse = {
    date: "20200129",
    sid: "2002",
    events: [
        {
            eid: "E7d2-22f3",
            st: 1580251200,
            d: 2700,
            t: "Young, Sikh and Proud",
            sy: "Sunny Hundal explores the legacy of his late brother Jagraj Singh, a prominent Sikh leader, and asks if a rise in religious activism points towards a bigger story of Sikh identity. Also in HD. [S]",
            eg: "News & Docs",
            esg: "Religious",
            programmeuuid: "2240b545-bad0-4055-a235-328ec7a79c70",
            r: "--",
            s: true,
            ad: false,
            hd: false,
            new: false
        }
    ]
};
