
import {mockServicesResponse, mockScheduleResponse} from "./data";
const API_URL = process.env.REACT_APP_API_URL;

export default {
  get: jest.fn((url) => {
    switch (url) {
      case `${API_URL}/services.json`:
        return Promise.resolve({data: mockServicesResponse});
      case `${API_URL}/schedule/20200129/2002.json`:
        return Promise.resolve({ data: mockScheduleResponse});
      default:
        return Promise.resolve(mockServicesResponse)

    }
  }),
};
